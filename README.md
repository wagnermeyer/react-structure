# **White Lion - Nova Arquitetura Frontend**

## Stack

| Ferramenta     | Descrição     |
|-|-|
| **ReactJS** | Framework JavaScript |
| **Node.js + Express** | Configurado para Server Side Rendering |
| **Apollo Client** | Cliente para consumir GraphQL e gerenciamento de estado |
| **Jest + Enzyme** | Ambiente de testes |

****
# Boas práticas

## **Convenções de nome no código**
Costumamos seguir algumas convenções de nome para deixar o código mais organizado e fácil de entender:

### **1. Variáveis**
Devem ser escritas utilizando o padrão _camelCase_, com no mínimo 3 caracteres, em inglês, de forma clara representando exatamente o valor em questão.
```javascript
// Errado
const qty = 1 // Quantidade de que?

// Correto
const ticketsQty = 1 // Representa claramente o valor de algo
```

#### - **Booleanos**
Variáveis com valores booleanos devem ser representados com prefixos `is`, `has`, `can`, sempre que possível. Dica: Faça a pergunta em inglês e responda sempre de forma positiva. Exemplos:
```javascript
// Is this a private event?
const isPrivate = true // Yes, it is private
const isPrivate = false // No, it isn't private

// Does this element have children?
const hasChildren = true // Yes, he has chidren
const hasChildren = false // No, he hasn't children

// Can this form submit?
const canSubmit = true // Yes, he can submit
const canSubmit = false // No, he can't submit
```

#### - **Arrays**
Variáveis que representam arrays devem sempre ser escritas no plural.
```javascript
// Errado
const ticket = [ {...}, {...}, {...} ]

// Correto
const tickets = [ {...}, {...}, {...} ]
```
Uma boa prática é indicar com um sufixo ou prefixo (priorize o sufixo) que a variável é um array.
```javascript
const ticketsArr = [ {...}, {...}, {...} ] // Recomendado
const arrTickets = [ {...}, {...}, {...} ]
```
_A prática acima deixa a utilização da variável mais clara e fácil de consumir. Quando não adotamos o prefixo/sufixo `arr`, entendemos que todas as variáveis na plural são arrays, mas, em alguns casos podemos utilizar variáveis no plural para definir uma string com diversas informações. Por exemplo:_
```javascript
// O nome no plural deveria ser um array, mas nesse caso precisamos utilizar um nome no plural para representar uma string.
const movies = "The Lord of The Rings, Star Wars, Avengers"

// Utilizando o prefixo 'arr', já fica evidente que estamos criando um array e deveremos consumir a variável de uma forma diferente da de cima.
const moviesArr = [
	"The Lord of The Rings",
	"Star Wars",
	"Avengers"
]
```

#### - **Objetos:**
Para definição de um `Objeto`, utilizamos a mesma prática de `Arrays`, sempre utilizando o prefixo/sufixo `Obj`. Ex.:
```javascript
const heroObj = { ... }
```
É aconselhado que, sempre que for identificado que haverá uma quantidade grande de variáveis com o mesmo contexto, seja agrupado em um objeto. Ex.:
```javascript
// Não recomendado
const heroName = "Spider-man"
const heroRealName = "Peter Parker"
const heroHability = "Spider sensor"
const heroAge = 17

// Errado
// Não é necessário repetir o contexto 'hero' em cada propriedade
const heroObj = {
	heroName = "Spider-man",
	heroRealName = "Peter Parker",
	heroHability = "Spider sensor",
	heroAge = 17
}

// Correto
const heroObj = {
	name: "Spider-man",
	realName: "Peter Parker",
	hability: "Spider sensor",
	age: 17
}
```

> **Dica geral:** Para manter a consistência na criação de varíaveis, utilize o padrão `contextoTipoDoDado`. Ex.:
```javascript
const heroName = "Spider-man" // 'hero' é o contexto e 'Name' é o tipo do dado
```

---

### **2. Classes**

Devem sempre ser escritas em `PascalCase`, no singular. Ex.:
```javascript
// Errado
class marvelHero {
	...
}

// Correto
class MarvelHero {
	...
}
```

### **3. Métodos/Funções**
Devem sempre ser escritas em `camelCase` e é importante que representem uma ação.
```javascript
// Errado
function hero() {
	...
}

// Correto
function getHero() {
	...
}
```

#### - **Métodos/Funções que retornam booleanos:**
Quando algum método ou função retornar booleano, não é necessário representar uma ação, podemos utilizar os prefixos `is`, `has`, `can`, da mesma maneira que utilizamos em varíaveis que representam booleanos.
```javascript
function isEnemy() {
	return true
}
```

---

## **Componentes**
### **Conceitos**
Antes de criar um novo componente, é necessário analisar e entender em quais dos casos o componente se encaixa:

## 1. Universal Component:
![Universal Components](1-universal-component.jpg)

É um componente criado pensando na portabilidade entre projetos. Devem funcionar de forma isolada, sem dependências do projeto.
> Futuramente, pretendemos mover esses componentes para um projeto a parte. Esses componentes serão instalados via NPM/Yarn.

## 2. Shared Component:
![Universal Components](2-shared-component.jpg)

É um componente compartilhado entre as telas/rotas/views do projeto.

## 3. Unique Component:
![Universal Components](3-unique-component.jpg)

É um componente exclusivo de alguma view/rota específica. É utilizado uma ou mais vezes dentro da uma view específica.

## 4. Child Component:
![Universal Components](4-child-component.jpg)

É um componente filho de outro componente. Muito comum em caso de componentes de listas. Ex.: List.jsx (pai), _ListItem.jsx (filho). É indicado com um underline antes do nome.

## 5. Styleguide Component:
![Universal Components](5-styleguide-component.jpg)

É um componente que é relacionado ao styleguide. Ex.: Button, Card, Heading

> http://react-file-structure.surge.sh/

## Criação de componentes
Os componentes devem sempre ser criados utilizando as seguintes regras:
- **Pasta:** Nome do componente utilizando PascalCase

- **Nome do arquivo .js:** Nome do arquivo igual ao da pasta com extensão `.jsx`

- **Nome da classe/função do componente:** Deve ser a mesma da pasta

- **Estilo do componente:** Nome do arquivo igual ao da pasta com extensão `.style.js`

- **Arquivo de teste:** Nome do arquivo igual ao da pasta com extensão `.test.js`. Todos os componentes devem conter um arquivo de teste com no mínimo um caso de teste.

- **Multi-linguagem:** Os componentes devem, obrigatoriamente, ser multi-linguagem. Para isso, toda pasta do componente deve ter uma pasta chamada `lang`, com um arquivo json, de mesmo nome da pasta, contendo um objeto com 3 linguagens: `PT`, `EN`, `ES`. Mais detalhes na seção "Multi-linguagem"


***
## **Views**
Utilizamos o conceito de "views" para criação das telas da aplicação. É importante que as seguintes regras sejam seguidas:
- Cada pasta dentro da pasta "views" deve representar um diretório do site. Ex.:
  - https://sympla.com.br/evento -> Pasta `/Event`
  - https://sympla.com.br/checkout -> Pasta `/Checkout`

- Subdiretórios devem ser subpastas, independente do nível, todos os subdiretórios da URL devem ser colocados no mesmo nível. Ex.:
  - https://sympla.com.br/checkout/etapa-1 -> Pasta `/Checkout/Step1`
  - https://sympla.com.br/checkout/etapa-1/etapa-2 -> Pasta `/Checkout/Step2`
  - Cada "view" deve seguir o mesmo padrão de criação de um componente. Ver "Criação de componentes"

- O primeiro nível da pasta `views` deve conter APENAS as pastas que representam cada diretório na URL + a pasta `_sharedComponents` (Ver Shared Components).

- Dentro da pasta de cada diretório, pode conter:
  - A pasta `_sharedComponents`, caso hajam pastas de subdiretórios
  - A pasta `_uniqueComponents`
  - Arquivos relacionados a view (`lang`, `.jsx`, `.style.js`, `.test.js`)
****

## **Utils**
Na pasta "utils" guardamos funções e classes que são úteis no projeto todo. Ainda precisamos desenvolver regras mais claras para a organização dessa pasta.

****

## **Multi-linguagem**
Por padrão, todos os componentes devem ser desenvolvidos suportando 3 linguas (Ver "Componentes > Criação de Componentes"). Para que o componente tenha acesso a linguagem selecionada pelo usuário, é necessário encapsulá-lo com o H.O.C. `withLang`, disponível em `./src/config/languageProvider`.

****

## **Documentação**
Todas as funções e métodos devem ser documentados em inglês, utilizando a sintaxe do JSDOC

Exemplo:
```javascript
	/**
	* @function isSameDate
	* @description - Compares two dates and checks if they are equal
	* @param {object} firstDate - The first date object you want to compare
	* @param {object} secondDate - The second date object you want to compare
	* @returns {boolean} - If the passed dates are equal, the function returns TRUE, otherwise, returns FALSE 
	*/
	export const isSameDate = ( firstDate, secondDate ) => {
	// Convert dates to string - - - - - - - - - - - - - - - - 
	const firstDateString = firstDate.toLocaleDateString()
	const secondDateString = secondDate.toLocaleDateString()
	// Date check  - - - - - - - - - - - - - - - - - - - - - -
	return firstDateString === secondDateString
	}

```